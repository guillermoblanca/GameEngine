#pragma once
#pragma once
#include "GB/ComponentSystem/GameObject.h"
#include "GB/Core.h"
#include "gbpch.h"
namespace GB
{
	class World
	{
		friend Actor;
	public:

		void Instantiate(Actor* actor, vector3 pos);
		void Instantiate(Actor* actor, vector3 pos, vector3 rot);
		void Instantiate(Actor* actor, vector3 pos, vector3 rot, Actor* parent);

		void Destroy(Actor* actor);
		void Destroy(Actor* actor, float delay);

		Actor* FindActorByName(std::string name);
		Actor* FindActorByTag(std::string name);

	private:
		std::vector<Actor*> actors;

	};
}